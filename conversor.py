import os
import moviepy.editor as mp
from pathlib import Path

armazenamento = os.path.dirname(os.path.realpath(__file__))
os.makedirs("Your_Songs",exist_ok=True)

local = str(input("Caminho dos vídeos: ")).strip()
extensao = str(input("Extensão: "))

for videos in os.listdir(str(local)):
	if videos.endswith(extensao):
		os.chdir(armazenamento)
		os.chdir("Your_Songs")
		seu_video = local + videos
		sua_musica = videos.replace(extensao,".mp3")
		escrever_musica = mp.VideoFileClip(seu_video)
		with open(sua_musica,"wb") as musica:
			os.path.join("Your_Songs"+"/"+str(escrever_musica.audio.write_audiofile(sua_musica)))
			musica.close()
